from sqlalchemy import create_engine
from models import GenericBase

database_url = 'sqlite:///./database.db'

engine = create_engine(database_url, connect_args={"check_same_thread": False})

from models import (
    Graduation,
    Student,
    Subject,
    TeacherModel,
    StudentSubject
)

GenericBase.metadata.create_all(bind=engine)
