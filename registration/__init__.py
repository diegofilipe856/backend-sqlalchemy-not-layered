from uuid import uuid4
from data_manipulation import *
from fastapi import FastAPI, HTTPException
from sqlalchemy.orm import Session
import uvicorn
from models import *

'''@app.put('',
         summary='',
         tags=[''])'''


def new_student_module(db: Session, student_data):
    db_student = Student(id=student_data.id, name=student_data.name, cpf=student_data.cpf, age=student_data.age,
                         rg=student_data.rg, dispatching_agency=student_data.dispatching_agency,
                         birthday=student_data.birthday, graduation_id=student_data.graduation_id)
    db.add(db_student)
    db.commit()
    db.refresh(db_student)
    return {'msg': "Estudante criado com sucesso! ;)"}


def new_graduation_module(db: Session, graduation_data):
    db_graduation = Graduation(id=graduation_data.id, name=graduation_data.name,
                               date_of_creation=graduation_data.date_of_creation,
                               coordinator=graduation_data.coordinator, building_name=graduation_data.building_name)

    db.add(graduation_data)
    db.commit()
    db.refresh(graduation_data)
    return {'msg': "Curso criado com sucesso! ;)"}


def new_teacher_module(db: Session, teacher_data):
    db_teacher = TeacherModel(id=teacher_data.id, name=teacher_data.name, cpf=teacher_data.cpf, titulation=teacher_data.titulation)
    db.add(db_teacher)
    db.commit()
    db.refresh(db_teacher)
    return {'msg': "Professor criado com sucesso! ;)"}


def new_subject_module(db: Session, subject_data):
    db_subject = Subject(id=subject_data.id, code=subject_data.code, name=subject_data.name,
                         teacher_id=subject_data.teacher_id, description=subject_data.description)
    verify_teacher = db.query(TeacherModel).filter(db_subject.teacher_id == TeacherModel.id).first()
    if verify_teacher:
        db.add(db_subject)
        db.commit()
        db.refresh(db_subject)
        return {"msg": "Disciplina criada com sucesso!"}
    else:
        return {'erro': "Professor não encontrado! :("}


def enroll_student_module(db: Session, data):
    student_cpf = data.studentCpf
    subject_code = data.subjectCode
    student = db.query(Student.id).filter(student_cpf == Student.cpf).first()
    subject = db.query(Subject.id).filter(subject_code == Subject.code).first()
    if student is not None and subject is not None:
        student = str(student[0])
        subject = str(subject[0])
        db_student_subject = StudentSubject(student_id=student, subject_id=subject)
        db.add(db_student_subject)
        db.commit()
        db.refresh(db_student_subject)
        db.close()
        return {'msg': 'Estudante matriculado com sucesso! ;)'}

    else:
        return {'erro': "Erro! O aluno e/ou disciplina não existe(m)."}


def unenroll_student_module(db: Session, data):
    student_cpf = data.studentCpf
    subject_code = data.subjectCode
    student = db.query(Student.id).filter(student_cpf == Student.cpf).first()
    subject = db.query(Subject.id).filter(subject_code == Subject.code).first()

    if student is not None and subject is not None:
        student = str(student[0])
        subject = str(subject[0])
        db_student_subject = StudentSubject(student_id=student, subject_id=subject)
        db.query(StudentSubject).filter(StudentSubject.student_id == db_student_subject.student_id).filter(StudentSubject.subject_id == db_student_subject.subject_id).delete()
        db.commit()
        return {'msg': "Estudante desmatriculado com sucesso! ;)"}

    else:
        return {'erro': "Erro! O aluno e/ou disciplina não existe(m)."}
