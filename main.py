import json
from registration import *
from read import *
from fastapi import FastAPI, HTTPException, Depends
import uvicorn
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from schemas import *

database_url = 'sqlite:///./database.db'

engine = create_engine(database_url, connect_args={"check_same_thread": False})

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_session():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


app = FastAPI(title="PROGRAD students program")


@app.post('/students/new-student',
          summary='Add new student',
          tags=['Students'],
          )
def new_student(student_data: Student, db: Session = Depends(get_session)):
    return new_student_module(db, student_data)


@app.post('/graduation/new-graduation',
          summary='Add new graduation',
          tags=['Graduations'])
def new_graduation(graduation_data: Graduation, db: Session = Depends(get_session)):
    return new_graduation_module(db, graduation_data)


@app.post('/teachers/new-teacher',
          summary='Adds new teacher',
          tags=['Teachers'])
def new_teacher(teacher_data: Teacher, db: Session = Depends(get_session)):
    return new_teacher_module(db, teacher_data)


@app.post('/subjects/new-subject',
          summary='Adds new subject',
          tags=['Subjects'],
          response_model=dict
          )
def new_subject(subject_data: Subject, db: Session = Depends(get_session)):
    return new_subject_module(db, subject_data)


@app.post('/students/enroll-student',
          summary='Enroll students into a subject',
          tags=['Students'])
def enroll_student(student_data: EnrollStudent, db: Session = Depends(get_session)):
    return enroll_student_module(db, student_data)


@app.delete('/students/unenroll-student',
            summary='Unenroll student',
            tags=['Students'])
def unenroll_student(student_data: EnrollStudent, db: Session = Depends(get_session)):
    return unenroll_student_module(db, student_data)


#   from read


@app.get('/students/catch-students',
         summary='Catch all students',
         tags=['Students'])
def rescue_all_students(db: Session = Depends(get_session)):
    return rescue_all_students_module(db)


@app.get('/students/catch-student/{id}',
         summary='Catch single student data',
         tags=['Students'])
def rescue_student(id: str, db: Session = Depends(get_session)):
    return rescue_student_module(db, id)


@app.get('/students/show-students-in-subject',
         summary='Show the students that belongs to a specific subject',
         tags=['Students'])
def students_in_subject(subject_code: str, db: Session = Depends(get_session)):
    return students_in_subject_module(db, subject_code)


@app.get('/subjects/ranking',
         summary='Show a ranking of subjects',
         tags=['Subjects'])
def ranking_of_subjects(db: Session = Depends(get_session)):
    return ranking_of_subjects_module(db)


if __name__ == '__main__':
    uvicorn.run(app='main:app', port=3000, reload=True)

