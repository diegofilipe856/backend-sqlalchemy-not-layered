from uuid import uuid4
from data_manipulation import *
from sqlalchemy.orm import Session
from models import *


def rescue_all_students_module(db: Session):
    return {'Students': db.query(Student).all()}


def rescue_student_module(db: Session, student_id):
    return {'student': db.query(Student).filter(Student.id == student_id).first()}


def students_in_subject_module(db: Session, subject):
    subject_id = db.query(Subject.id).filter(Subject.code == subject).first()
    subject_id = subject_id[0]
    all_students = db.query(StudentSubject.student_id).filter(StudentSubject.subject_id == subject_id).all()
    all_students = all_students
    list_of_names = []
    for i in all_students:
        i = i[0]
        temp_name = db.query(Student.name).filter(Student.id == i).first()
        list_of_names.append(temp_name)

    return {'msg': "Aqui você verá os estudantes matriculados numa disciplina! :D",
            'prompt': list_of_names}


def ranking_of_subjects_module(db: Session):
    subjects = db.query(Subject.id).all()
    array_of_subjects = []
    for subject in subjects:
        subject = subject[0]
        code = db.query(Subject.code).filter(Subject.id == subject).first()
        code = code[0]
        temp_dict = {}
        temp_dict['code'] = code
        number_of_students = db.query(StudentSubject.student_id).filter(StudentSubject.subject_id == subject).all()
        print('-' * 5)
        print(number_of_students)
        print('-' * 5)
        temp_dict["number_of_students"] = len(number_of_students)
        array_of_subjects.append(temp_dict)
    array_of_subjects = sorted(array_of_subjects, key=lambda x: x['number_of_students'], reverse=True)
    return {'msg': 'Aqui você verá um ranking de disciplinas! :D',
            'prompt': array_of_subjects}
