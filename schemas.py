from pydantic import BaseModel, Field
from typing import List


def snake_to_camel(snake_str: str | None = None):
    if snake_str is None:
        return None
    splitted = snake_str.split('_')
    return splitted[0] + ''.join(s.title() for s in splitted[1:])


class GenericModel(BaseModel):
    class Config:
        alias_generator = snake_to_camel
        allow_population_by_field_name = True


class BaseGraduation(GenericModel):
    id: str
    name: str


class Graduation(BaseGraduation):
    date_of_creation: str
    coordinator: str
    building_name: str


class StudentBase(GenericModel):
    id: str = Field(title='Student ID: ', example='UUID')


class Student(StudentBase):
    name: str = Field(title='Nome do estudante: ', example='Douglas')
    cpf: str = Field(title='CPF: ', example='123.456.789-01')
    age: int = Field(title='Idade: ', example='19')
    rg: str = Field(title='RG: ', example='12.345.678')
    dispatching_agency: str = Field(title='Órgão expeditor: ', example='SDS')
    birthday: str = Field(title='Data de aniversário: ', example='15/01')
    graduation_id: str = Field(title='ID da graduação: ', example='UUID')


class EnrollStudent(GenericModel):
    studentCpf: str = Field(title='Digite o CPF do estudante: ', example='000.000.000.-00')
    subjectCode: str = Field(title='Digite o código da disciplina que o estudante será matriculado: ', example='EQUI000')


class EnrollStudentdb(GenericModel):
    studentId: str
    subjectId: str


class BaseSubject(GenericModel):
    id: str
    code: str
    name: str
    teacher_id: str


class Subject(BaseSubject):
    description: str


class BaseTeacher(GenericModel):
    id: str


class Teacher(BaseTeacher):
    name: str
    cpf: str
    titulation: str

