from sqlalchemy import Boolean, Integer, String, Column, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class GenericBase(Base):
    __abstract__ = True


class GraduationBase(GenericBase):
    __tablename__ = "Graduations"
    id = Column(String, primary_key=True)
    name = Column(String, unique=True)


class Graduation(GraduationBase):
    date_of_creation = Column(String, default=None)
    coordinator = Column(String, ForeignKey("Teachers.id"))
    building_name = Column(String, default=None)

    TeacherModel = relationship("TeacherModel", back_populates="Graduation")
    Student = relationship("Student", back_populates="Graduation")


class Student(GenericBase):
    __tablename__ = "Students"

    id = Column(String, primary_key=True)
    name = Column(String)
    cpf = Column(String, unique=True)
    age = Column(Integer, default=None)
    rg = Column(String)
    dispatching_agency = Column(String)
    birthday = Column(String, default=None)
    graduation_id = Column(String, ForeignKey("Graduations.id"))
#   items = relationship("Item", back_populates="owner")

    Subject = relationship("Subject", secondary='Student_subject', back_populates="Student")
    Graduation = relationship("Graduation", back_populates="Student")


class StudentSubject(GenericBase):
    __tablename__ = 'Student_subject'

    enrollment_id = Column(Integer, primary_key=True, autoincrement=True)

    student_id = Column(String, ForeignKey("Students.id"), nullable=False)
    subject_id = Column(String, ForeignKey("Subjects.id"), nullable=False)

    __table_args__ = (UniqueConstraint("student_id", "subject_id"),)


class Subject(GenericBase):
    __tablename__ = "Subjects"

    id = Column(String, primary_key=True, nullable=False)
    code = Column(String, nullable=False)
    name = Column(String, nullable=False)
    teacher_id = Column(String, ForeignKey("Teachers.id"), nullable=False)
    description = Column(String, default=None)

    Student = relationship("Student", secondary='Student_subject', back_populates="Subject")
    TeacherModel = relationship("TeacherModel", back_populates="Subject")


class TeacherModel(GenericBase):
    __tablename__ = "Teachers"

    id = Column(String, primary_key=True)
    name = Column(String)
    cpf = Column(String, unique=True)
    titulation = Column(String)

    Subject = relationship("Subject", back_populates="TeacherModel")
    Graduation = relationship("Graduation", back_populates="TeacherModel")

